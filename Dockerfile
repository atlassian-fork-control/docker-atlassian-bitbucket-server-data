FROM debian:jessie

ENV BITBUCKET_HOME /var/atlassian/application-data/bitbucket

RUN mkdir -p "${BITBUCKET_HOME}" \
    mkdir    "${BITBUCKET_HOME}/lib" \
    && chmod -R 700 "${BITBUCKET_HOME}" \
    && chown -R daemon:daemon "${BITBUCKET_HOME}"
  
USER daemon

VOLUME ${BITBUCKET_HOME}

CMD ["echo", "Data container for Atlassian Bitbucket"]
